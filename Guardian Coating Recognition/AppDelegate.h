//
//  AppDelegate.h
//  Guardian Coating Recognition
//
//  Created by Алексей on 03.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

