//
//  main.m
//  Guardian Coating Recognition
//
//  Created by Алексей on 03.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
