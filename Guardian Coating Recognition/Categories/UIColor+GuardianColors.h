//
//  UIColor+GuardianColors.h
//  Guardian Coating Recognition
//
//  Created by Алексей on 04.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (GuardianColors)

+ (UIColor *)guardianBlueColor;

@end
