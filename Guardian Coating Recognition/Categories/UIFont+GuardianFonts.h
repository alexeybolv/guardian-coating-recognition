//
//  UIFont+GuardianFonts.h
//  Guardian Coating Recognition
//
//  Created by Алексей on 04.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (GuardianFonts)

+ (UIFont *)sfUITextRegularWithSize:(CGFloat)fontSize;
+ (UIFont *)sfUITextBoldWithSize:(CGFloat)fontSize;

@end
