//
//  UIColor+GuardianColors.m
//  Guardian Coating Recognition
//
//  Created by Алексей on 04.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import "UIColor+GuardianColors.h"

@implementation UIColor (GuardianColors)

+ (UIColor *)guardianBlueColor {
    UIColor *color = [UIColor colorWithRed:93.0/255.0 green:168.0/255.0 blue:255.0/255.0 alpha:1.0];
    return color;
}

@end
