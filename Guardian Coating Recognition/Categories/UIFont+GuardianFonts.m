//
//  UIFont+GuardianFonts.m
//  Guardian Coating Recognition
//
//  Created by Алексей on 04.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import "UIFont+GuardianFonts.h"

@implementation UIFont (GuardianFonts)

+ (UIFont *)sfUITextRegularWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@"SFUIText-Regular" size:fontSize];
    return font;
}

+ (UIFont *)sfUITextBoldWithSize:(CGFloat)fontSize {
    UIFont *font = [UIFont fontWithName:@"SFUIText-Bold" size:fontSize];
    return font;
}

@end
