//
//  CGRCameraCollectionViewCell.m
//  Guardian Coating Recognition
//
//  Created by Алексей on 04.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import "GCRCameraCollectionViewCell.h"

//Categories
#import "UIFont+GuardianFonts.h"

@implementation GCRCameraCollectionViewCell

- (void)prepareForReuse {
    [super prepareForReuse];
    self.mainLabel.textColor = [UIColor whiteColor];
    self.mainLabel.font = [UIFont sfUITextRegularWithSize:14.0];
}

@end
