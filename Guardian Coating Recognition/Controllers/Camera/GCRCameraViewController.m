//
//  GCRCameraViewController.m
//  Guardian Coating Recognition
//
//  Created by Алексей on 03.01.17.
//  Copyright © 2017 Fingers Media. All rights reserved.
//

#import "GCRCameraViewController.h"

//Views
#import "GCRCameraCollectionViewCell.h"

//Categories
#import "UIFont+GuardianFonts.h"
#import "UIColor+GuardianColors.h"

//Libs
#import <AVFoundation/AVFoundation.h>

@interface GCRCameraViewController () <AVCapturePhotoCaptureDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *cameraSettingsView;
@property (weak, nonatomic) IBOutlet UIButton *cameraButton;
@property (weak, nonatomic) IBOutlet UIButton *isoButton;
@property (weak, nonatomic) IBOutlet UIButton *shutterSpeedButton;
@property (weak, nonatomic) IBOutlet UIButton *chevronButton;
@property (weak, nonatomic) IBOutlet UIButton *galleryButton;
@property (strong, nonatomic) UIImageView *frameMirrorImageView;
@property (strong, nonatomic) UIImageView *frameFlameImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rectangleImageView;
@property (weak, nonatomic) IBOutlet UICollectionView *settingsCollectionView;

@property (strong, nonatomic) AVCaptureSession *session;
@property (strong, nonatomic) AVCaptureDevice *captureDevice;
@property (strong, nonatomic) AVCaptureVideoPreviewLayer *previewLayer;
@property(nonatomic, retain) AVCapturePhotoOutput *capturePhotoOutput;
@property (weak, nonatomic) IBOutlet UIView *cameraView;

@property (assign, nonatomic) BOOL shouldSavePhoto;
@property (assign, nonatomic, getter=isFocusRectVisible) BOOL focusRectVisible;
@property (assign, nonatomic) CGFloat rotateAngle;
@property (strong, nonatomic) NSIndexPath *selectedISOIndexPath;
@property (assign, nonatomic) CGRect selectedISOCellRect;
@property (strong, nonatomic) NSIndexPath *selectedShutterSpeedIndexPath;
@property (assign, nonatomic) CGRect selectedShutterSpeedCellRect;

@property (assign, nonatomic) float currentISO;
@property (assign, nonatomic) float currentShutterSpeed;

@end

@implementation GCRCameraViewController

static const float rectangleForControlsWidth = 217.0 - 52.0;
static const float defaultOffset = 8.0;
static const float collectionViewCellWidth = 45;
static const float collectionViewCellHeight = 40;
static const float collectionViewLeftOffset = 30;
static const int focusViewTag = 99;
static const int isoSelectionStep = 5;
static const float shutterSpeedStep = 0.001;

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self addNotifications];
    [self setUpProperties];
    [self initialCameraSetUp];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setUpOutlets];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (BOOL)shouldAutorotate {
    return NO;
}

#pragma mark - Touches

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    if (!self.isFocusRectVisible) {
        self.focusRectVisible = YES;
        CGPoint touchPoint = [[touches anyObject] locationInView:self.cameraView];
        double focus_x = touchPoint.x/self.cameraView.frame.size.width;
        double focus_y = touchPoint.y/self.cameraView.frame.size.height;
        NSError *error = nil;
        CGPoint point = CGPointMake(focus_y, focus_x);
        if ([self.captureDevice isFocusModeSupported:AVCaptureFocusModeContinuousAutoFocus] && [self.captureDevice lockForConfiguration:&error]){
            [self.captureDevice setFocusPointOfInterest:point];
            CGFloat focusRectSide = 60;
            CGRect rect = CGRectMake(touchPoint.x-focusRectSide/2, touchPoint.y-focusRectSide/2, focusRectSide, focusRectSide);
            UIView *focusRect = [[UIView alloc] initWithFrame:rect];
            focusRect.layer.borderColor = [UIColor whiteColor].CGColor;
            focusRect.layer.borderWidth = 2;
            focusRect.tag = focusViewTag;
            [self.cameraView addSubview:focusRect];
            [NSTimer scheduledTimerWithTimeInterval: 1
                                             target: self
                                           selector: @selector(dismissFocusRect)
                                           userInfo: nil
                                            repeats: NO];
            [self.captureDevice setFocusMode:AVCaptureFocusModeAutoFocus];
            [self.captureDevice unlockForConfiguration];
        }
    }
}

- (void) dismissFocusRect{
    for (UIView *subView in self.cameraView.subviews) {
        if (subView.tag == focusViewTag) {
            [subView removeFromSuperview];
            self.focusRectVisible = NO;
        }
    }
}

#pragma mark - Camera SetUp

- (void)initialCameraSetUp {
    self.session = [[AVCaptureSession alloc] init];
    self.captureDevice = [AVCaptureDevice defaultDeviceWithDeviceType:AVCaptureDeviceTypeBuiltInWideAngleCamera mediaType:AVMediaTypeVideo position:AVCaptureDevicePositionBack];
    self.currentShutterSpeed = CMTimeGetSeconds(self.captureDevice.activeFormat.minExposureDuration) + 1 * shutterSpeedStep;
    self.selectedShutterSpeedIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    self.currentISO = self.captureDevice.activeFormat.minISO;
    self.selectedISOIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    if (self.captureDevice) {
        [self beginSession];
    }
}

- (void)beginSession {
    [self configureDevice];
    NSError *error = nil;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:self.captureDevice error:&error];
    if ([self.session canAddInput:input]) {
        [self.session addInput:input];
    } else {
        NSLog(@"can't add input");
    }
    self.capturePhotoOutput = [[AVCapturePhotoOutput alloc] init];
    if ([self.session canAddOutput:self.capturePhotoOutput]) {
        [self.session addOutput:self.capturePhotoOutput];
    } else {
        NSLog(@"can't add output");
    }
    if (error != nil) {
        NSLog(@"error: %@",error.localizedDescription);
    }
    self.previewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:self.session];
    [self.cameraView.layer addSublayer:self.previewLayer];
    self.previewLayer.frame = self.cameraView.layer.frame;
    [self.session startRunning];
    
    [self changeCameraSettingsWithShutterSpeed:self.currentShutterSpeed andISO:self.currentISO];
    [self.capturePhotoOutput capturePhotoWithSettings:[AVCapturePhotoSettings photoSettings] delegate:self];
}

- (void)configureDevice {
    if (self.captureDevice) {
        [self.captureDevice lockForConfiguration:nil];
        self.captureDevice.focusMode = AVCaptureFocusModeContinuousAutoFocus;
        [self.captureDevice unlockForConfiguration];
    }
}

#pragma mark - AVCapturePhotoCaptureDelegate

-(void)captureOutput:(AVCapturePhotoOutput *)captureOutput didFinishProcessingPhotoSampleBuffer:(CMSampleBufferRef)photoSampleBuffer previewPhotoSampleBuffer:(CMSampleBufferRef)previewPhotoSampleBuffer resolvedSettings:(AVCaptureResolvedPhotoSettings *)resolvedSettings bracketSettings:(AVCaptureBracketedStillImageSettings *)bracketSettings error:(NSError *)error {
    
    if (error) {
        NSLog(@"error : %@", error.localizedDescription);
    }
    
    if (photoSampleBuffer && self.shouldSavePhoto) {
        self.shouldSavePhoto = NO;
        NSData *data = [AVCapturePhotoOutput JPEGPhotoDataRepresentationForJPEGSampleBuffer:photoSampleBuffer previewPhotoSampleBuffer:previewPhotoSampleBuffer];
        UIImage *image = [UIImage imageWithData:data];
        NSLog(@"image = %@", image);
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.isoButton.selected) {
        return (self.captureDevice.activeFormat.maxISO - self.captureDevice.activeFormat.minISO)/isoSelectionStep + 1;
    } else if (self.shutterSpeedButton.selected) {
        NSString *str = [NSString stringWithFormat:@"%f",CMTimeGetSeconds(self.captureDevice.activeFormat.maxExposureDuration) - CMTimeGetSeconds(self.captureDevice.activeFormat.minExposureDuration)];
        NSArray *arr = [str componentsSeparatedByString:@"."];
        return [[arr lastObject] intValue] * shutterSpeedStep;
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GCRCameraCollectionViewCell *cameraCollectionViewCell = (GCRCameraCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:[GCRCameraCollectionViewCell description] forIndexPath:indexPath];
    [self setUpCameraCollectionViewCell:cameraCollectionViewCell atIndexPath:indexPath];
    return cameraCollectionViewCell;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self deselectSettingsCell];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        [self selectSettingsCell];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self selectSettingsCell];
}

#pragma mark - Actions

- (IBAction)isoAction:(id)sender {
    self.shutterSpeedButton.selected = NO;
    ((UIButton *)sender).selected = !((UIButton *)sender).selected;
    if (((UIButton *)sender).selected) {
        self.cameraSettingsView.hidden = NO;
        if (self.selectedISOIndexPath) {
            [self.settingsCollectionView scrollRectToVisible:self.selectedISOCellRect animated:YES];
            [self.settingsCollectionView reloadData];
        }
    } else {
        self.cameraSettingsView.hidden = YES;
    }
}

- (IBAction)shutterSpeedAction:(id)sender {
    self.isoButton.selected = NO;
    ((UIButton *)sender).selected = !((UIButton *)sender).selected;
    if (((UIButton *)sender).selected) {
        self.cameraSettingsView.hidden = NO;
        if (self.selectedShutterSpeedIndexPath) {
            [self.settingsCollectionView scrollRectToVisible:self.selectedShutterSpeedCellRect animated:YES];
            [self.settingsCollectionView reloadData];
        }
    } else {
        self.cameraSettingsView.hidden = YES;
    }
}

- (IBAction)photoAction:(id)sender {
    self.shouldSavePhoto = YES;
    [self.capturePhotoOutput capturePhotoWithSettings:[AVCapturePhotoSettings photoSettings] delegate:self];
}

- (IBAction)openGalleryAction:(id)sender {
}

#pragma mark - Notifications

- (void)addNotifications {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didChangeOrientation:) name:UIDeviceOrientationDidChangeNotification object:nil];
}

- (void)didChangeOrientation:(NSNotification *)notification {
    UIDeviceOrientation orientation = [[UIDevice currentDevice] orientation];
    switch (orientation) {
        case UIDeviceOrientationLandscapeLeft: {
            [self setUpLandscapeLeftOrientation];
            self.rotateAngle = M_PI_2;
            [self.settingsCollectionView reloadData];
            break;
        }
        case UIDeviceOrientationLandscapeRight: {
            [self setUpLandscapeRightOrientation];
            self.rotateAngle = - M_PI_2;
            [self.settingsCollectionView reloadData];
            break;
        }
        case UIDeviceOrientationPortrait: {
            [self setUpPortraitOrientation];
            self.rotateAngle = 0;
            [self.settingsCollectionView reloadData];
            break;
        }
        default: {
            break;
        }
    }
    [UIView animateWithDuration:.3 animations:^{
        self.cameraButton.transform = CGAffineTransformMakeRotation(self.rotateAngle);
        self.isoButton.transform = CGAffineTransformMakeRotation(self.rotateAngle);
        self.shutterSpeedButton.transform = CGAffineTransformMakeRotation(self.rotateAngle);
    } completion:nil];
}

#pragma mark - Help Methods

- (void)setUpProperties {
    CGRect firstCellRect = CGRectMake(0, 0, collectionViewCellWidth, collectionViewCellHeight);
    self.selectedShutterSpeedCellRect = firstCellRect;
    self.selectedISOCellRect = firstCellRect;
}

- (void)setUpOutlets {
    self.settingsCollectionView.contentInset = UIEdgeInsetsMake(0, CGRectGetWidth(self.view.frame)/2 - collectionViewCellWidth, 0, CGRectGetWidth(self.view.frame)/2 - collectionViewCellWidth);
    self.frameMirrorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"frameMirror"]];
    [self.view addSubview:self.frameMirrorImageView];
    self.frameFlameImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"frameFlame"]];
    [self.view addSubview:self.frameFlameImageView];
    [self setUpPortraitOrientation];
}

- (void)setUpPortraitOrientation {
    [self.frameMirrorImageView setFrame:CGRectMake(defaultOffset, (CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3, CGRectGetWidth(self.view.frame)/3*1.7, ((CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3*1.5))];
    [self.frameFlameImageView setFrame:CGRectMake(16 + CGRectGetWidth(self.frameMirrorImageView.frame), (CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3, CGRectGetWidth(self.view.frame)/3*1.2 - defaultOffset*2, ((CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3*1.5))];
}

- (void)setUpLandscapeLeftOrientation {
    [self.frameMirrorImageView setFrame:CGRectMake(defaultOffset, defaultOffset*2, CGRectGetWidth(self.view.frame) - defaultOffset*2, ((CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3*2) - defaultOffset*2)];
    [self.frameFlameImageView setFrame:CGRectMake(defaultOffset, CGRectGetHeight(self.frameMirrorImageView.frame) + defaultOffset*3, CGRectGetWidth(self.view.frame) - defaultOffset*2, ((CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3*1) - defaultOffset*3)];
}

- (void)setUpLandscapeRightOrientation {
    [self.frameFlameImageView setFrame:CGRectMake(defaultOffset, defaultOffset*2, CGRectGetWidth(self.view.frame) - defaultOffset*2, ((CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3*1) - defaultOffset*3)];
    [self.frameMirrorImageView setFrame:CGRectMake(defaultOffset, CGRectGetHeight(self.frameFlameImageView.frame) + defaultOffset*3, CGRectGetWidth(self.view.frame) - defaultOffset*2, ((CGRectGetHeight(self.view.frame) - rectangleForControlsWidth)/3*2) - defaultOffset*2)];
}

- (void)selectSettingsCell {
    CGPoint centerPoint = CGPointMake(self.settingsCollectionView.center.x + self.settingsCollectionView.contentOffset.x - collectionViewLeftOffset,
                                      self.settingsCollectionView.center.y + self.settingsCollectionView.contentOffset.y);
    NSIndexPath *centerIndexPath = [self.settingsCollectionView indexPathForItemAtPoint:centerPoint];
    if (!centerIndexPath) {
        centerPoint.x += 25;
        centerIndexPath = [self.settingsCollectionView indexPathForItemAtPoint:centerPoint];
    }
    GCRCameraCollectionViewCell *cameraCollectionViewCell = (GCRCameraCollectionViewCell *)[self.settingsCollectionView cellForItemAtIndexPath:centerIndexPath];
    [self.settingsCollectionView scrollRectToVisible:cameraCollectionViewCell.frame animated:YES];
    cameraCollectionViewCell.mainLabel.font = [UIFont sfUITextBoldWithSize:13.0];
    cameraCollectionViewCell.mainLabel.textColor = [UIColor guardianBlueColor];
    if (self.isoButton.selected) {
        self.selectedISOIndexPath = centerIndexPath;
        self.selectedISOCellRect = cameraCollectionViewCell.frame;
        self.currentISO = self.captureDevice.activeFormat.minISO + centerIndexPath.item * isoSelectionStep;
    } else {
        self.selectedShutterSpeedIndexPath = centerIndexPath;
        self.selectedShutterSpeedCellRect = cameraCollectionViewCell.frame;
        self.currentShutterSpeed = CMTimeGetSeconds(self.captureDevice.activeFormat.minExposureDuration) + (centerIndexPath.item + 1) * shutterSpeedStep;
    }
    [self changeCameraSettingsWithShutterSpeed:self.currentShutterSpeed andISO:self.currentISO];
}

- (void)deselectSettingsCell {
    CGPoint centerPoint = CGPointMake(self.settingsCollectionView.center.x + self.settingsCollectionView.contentOffset.x - collectionViewLeftOffset,
                                      self.settingsCollectionView.center.y + self.settingsCollectionView.contentOffset.y);
    NSIndexPath *centerIndexPath = [self.settingsCollectionView indexPathForItemAtPoint:centerPoint];
    if (!centerIndexPath) {
        centerPoint.x += 25;
        centerIndexPath = [self.settingsCollectionView indexPathForItemAtPoint:centerPoint];
    }
    GCRCameraCollectionViewCell *cameraCollectionViewCell = (GCRCameraCollectionViewCell *)[self.settingsCollectionView cellForItemAtIndexPath:centerIndexPath];
    cameraCollectionViewCell.mainLabel.font = [UIFont sfUITextRegularWithSize:12.0];
    cameraCollectionViewCell.mainLabel.textColor = [UIColor whiteColor];
}

- (void)setUpCameraCollectionViewCell:(GCRCameraCollectionViewCell *)cameraCollectionViewCell atIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isoButton.selected) {
        cameraCollectionViewCell.mainLabel.text = [NSString stringWithFormat:@"%ld",(int)self.captureDevice.activeFormat.minISO + indexPath.item * isoSelectionStep];
    } else {
        float currentValue = (CMTimeGetSeconds(self.captureDevice.activeFormat.minExposureDuration) + indexPath.item + 1) * shutterSpeedStep;
        cameraCollectionViewCell.mainLabel.text = [NSString stringWithFormat:@"%.0f\nms",currentValue * 1000];
    }
    cameraCollectionViewCell.transform = CGAffineTransformMakeRotation(self.rotateAngle);
    if (self.isoButton.selected && self.selectedISOIndexPath == indexPath) {
        cameraCollectionViewCell.mainLabel.font = [UIFont sfUITextBoldWithSize:12.0];
        cameraCollectionViewCell.mainLabel.textColor = [UIColor guardianBlueColor];
    } else  if (self.shutterSpeedButton.selected && self.selectedShutterSpeedIndexPath == indexPath){
        cameraCollectionViewCell.mainLabel.font = [UIFont sfUITextBoldWithSize:12.0];
        cameraCollectionViewCell.mainLabel.textColor = [UIColor guardianBlueColor];
    } else {
        cameraCollectionViewCell.mainLabel.font = [UIFont sfUITextRegularWithSize:12.0];
        cameraCollectionViewCell.mainLabel.textColor = [UIColor whiteColor];
    }
}

- (void)changeCameraSettingsWithShutterSpeed:(float)shutterSpeed andISO:(float)iso {
    NSError *error = nil;
    if ([self.captureDevice lockForConfiguration:&error]) {
        [self.captureDevice setExposureModeCustomWithDuration:CMTimeMake(shutterSpeed*100000, 100000) ISO:iso completionHandler:nil];
        [self.captureDevice unlockForConfiguration];
    } else {
        NSLog(@"%@", error);
    }
}

@end
